class CreateInvitations < ActiveRecord::Migration
  def change
    create_table :invitations do |t|
      t.integer :user_id
      t.string :key
      t.string :friends_email
      t.string :status

      t.timestamps
    end

    add_index :invitations, :key, :unique => true
  end
end
