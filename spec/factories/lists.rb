#:user_id, :day, :list_items, :list_items_attributes

FactoryGirl.define do
  factory :list do |f|
    f.association(:user)
    f.day Date.today
  end
end