#  attr_accessible :friends_email, :key, :status, :user_id

FactoryGirl.define do
  factory :invitation do |f|
    f.friends_email Faker::Internet.email
    f.association(:user)
  end
end