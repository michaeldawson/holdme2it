require 'spec_helper'

describe Invitation do

	it "has a valid factory" do
		FactoryGirl.create(:invitation).should be_valid
	end

	it "rejects an empty email" do
		FactoryGirl.build(:invitation, :friends_email=>"").should_not be_valid
	end

	it "rejects an invalid email" do
		FactoryGirl.build(:invitation, :friends_email=>"tim.google.com").should_not be_valid
	end

	it "generates a random unique key on creation" do
		@invitation = FactoryGirl.create(:invitation)
		@invitation.key.should be_kind_of(String)
	end

	it "is assigned to the 'init' status on creation" do
		FactoryGirl.build(:invitation).status.should == 'init'
	end

	it "emails the friend after creation" do
		expect {FactoryGirl.create(:invitation)}.to change{ActionMailer::Base.deliveries.count}.by(1)
	end

	it "assigns both partners on acceptance" do
		@user=FactoryGirl.create(:user)
		@friends_email = Faker::Internet.email
		@invitation = FactoryGirl.create(:invitation, :user=>@user, :friends_email => @friends_email)

		@friend=FactoryGirl.create(:user, :email=>@friends_email)

		@invitation.accept(@friend.id)

		@user.reload
		@friend.reload
		
		@user.partner.should == @friend
		@friend.partner.should == @user
	end

end
