require 'spec_helper'

describe User do
	it "has a valid factory" do
		FactoryGirl.create(:user).should be_valid
	end

	context "a user" do
		before :each do 
			@user = FactoryGirl.create(:user)
		end

		context "with a current list" do
			before :each do
				@list = FactoryGirl.create(:list, :user_id=>@user.id, :day=>Date.today)
			end

			it "returns the current list" do
				@user.current_list.should == @list
			end
		end

		context "without a current list" do
			before :each do
				@list = FactoryGirl.create(:list, :user_id=>@user.id, :day=>Date.yesterday)
			end

			it "returns a new, current list" do
				@user.current_list.should be_kind_of(List)
				@user.current_list.should_not == @list
				@user.current_list.day.should ==Date.today
			end
		end

		context "without a partner" do
			it "should return nil for the partner" do
				@user.partner.should be_nil
			end
		end

		context "with a partner" do 
			before :each do
				@partner = FactoryGirl.create(:user)
				@user.partner=@partner
				@user.save
			end

			it "should return the partner" do
				@user.partner.should == @partner
			end
		end
	end
end