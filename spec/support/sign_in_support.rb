module ValidUserHelper
  def sign_in_as_a_valid_user
    @user ||= FactoryGirl.create :user
    sign_in @user # method from devise:TestHelpers
    @user
  end
end
