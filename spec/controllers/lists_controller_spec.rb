require 'spec_helper'

describe ListsController do

	describe 'GET #show' do
		context "when a user isn't logged in" do
			it "redirects to the sign up page" do
				get 'show'
				response.should be_redirect
			end
		end

		context "when a user is logged in" do
			before :each do
				@user=sign_in_as_a_valid_user
			end

			context "when the user doesn't have a partner" do
				it "sets the user variable but not the partner variable" do
					get 'show'
					assigns(:user).should == @user
					assigns(:partner).should be_nil
				end

				it "shows the lists page" do
					get 'show' 
					response.should be_success
				end
			end

			context "when the user has a partner" do
				before :each do
					@partner = FactoryGirl.create(:user)
					@user.partner=@partner
					@user.save
				end

				it "sets the user variable and the partner variable" do
					get 'show'
					assigns(:user).should == @user
					assigns(:partner).should == @partner
				end

				it "shows the lists page" do
					get 'show' 
					response.should be_success
				end
			end
		end
	end

end
