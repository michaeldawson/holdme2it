class CustomFailure < Devise::FailureApp
  def redirect_url

    if warden_options[:scope] == :user

      if warden_options[:attempted_path]=='/login'
        new_user_session_path
      else
        new_user_registration_path
      end
    else
      new_admin_user_session_path
    end
  end

  def respond
    if http_auth?
      http_auth
    else
      redirect
    end
  end
end