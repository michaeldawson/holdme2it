class InvitationsController < InheritedResources::Base
before_filter :authenticate_user!, :only=> [:create, :accept, :cancel]

	def create
		@friends_email=params[:invitation][:friends_email]

		if @friends_email == current_user.email
			flash[:error]="You can't invite yourself, silly!"
		else

			@invitation = Invitation.new(:friends_email=>@friends_email, :user_id => current_user.id)
			if @invitation.save
				flash[:notice]="Thanks, we've invited your friend!"
				UserNotifier.send_invitation_email(@invitation).deliver
			else
				flash[:error]="There was a problem inviting your friend."
			end
		end

		redirect_to lists_path
	end

	def view
		#redirects to the lists page from where the user can create their account

		@invitation = Invitation.find_by_key(params[:key]) if params[:key]

		if @invitation
			case @invitation.status 
			when "sent"
				if user_signed_in?
					redirect_to lists_path
				end
			when "cancelled"
				flash[:error]="Sorry, but that invitation's been cancelled!"
				redirect_to root_path
			when "accepted"
				flash[:error]="Sorry, but that invitation's already been accepted!"
				redirect_to root_path
			end
		else
			flash[:error]="Sorry, that link doesn't seem to be valid."
			redirect_to root_path
		end
	end

	def accept
		@invitation = Invitation.find_by_key(params[:key]) if params[:key]

		if @invitation && (current_user.invitation == @invitation || current_user.email == @invitation.friends_email)
			@invitation.accept(current_user.id)
			flash[:success]="You've accepted #{@invitation.user.first_name}'s invitation!"
			redirect_to lists_path
		else
			flash[:error]="Sorry, that link doesn't seem to be valid."
			redirect_to root_path
		end
	end

	def cancel
		@invitation = Invitation.find_by_key(params[:key]) if params[:key]

		if @invitation && current_user == @invitation.user
			@invitation.cancel
			redirect_to lists_path
			flash[:success]="We've cancelled that invitation - the link will no longer work."
		else
			flash[:error]="Sorry, that link doesn't seem to be valid."
			redirect_to lists_path
		end
	end
end
