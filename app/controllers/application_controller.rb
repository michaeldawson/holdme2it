class ApplicationController < ActionController::Base
  protect_from_forgery

	def after_sign_in_path_for(resource)
		#check_and_create_path

		if current_user.invitation_id && !current_user.partner
			@invitation=Invitation.find(current_user.invitation_id)
			accept_invitation_path(@invitation.key)
		else
			lists_path
		end
	end
end
