class ListsController < InheritedResources::Base
	before_filter :authenticate_user!

	def show
		@user=current_user
		@partner=current_user.partner

		@sent_invitations = current_user.sent_invitations.reject{|i| i.status=="cancelled"}
	end

	def update
		@list = List.find(params[:id])
		if @list.update_attributes(params[:list])
		  redirect_to lists_path, :notice  => "Successfully updated list."
		else
		  render :action => 'edit'
		end
	end

	def data
		if params[:id] && List.find(params[:id])
			list=List.find(params[:id])
			render '_list', :locals=>{:list=>list, :@removable=>(user_signed_in? && current_user.current_list.id==list.id)}, :layout=>false
		else
			render :nothing
		end
	end
end
