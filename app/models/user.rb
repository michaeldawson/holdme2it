class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me
  attr_accessible :first_name, :last_name, :invitation_id, :partner_id

  belongs_to :partner, :class_name=>:User
  validates :first_name, :presence=> true

  has_many :lists
  has_many :sent_invitations, :class_name=>:Invitation
  belongs_to :invitation

  def current_list
    if self.lists.last && self.lists.last.day == Date.today
    #the last list day is the current one
      self.lists.last
  	else
  		last_list=self.lists.last
      list=List.create(:user_id=>self.id, :day=>Date.today)

      if last_list && last_list.incomplete_list_items
        list.list_items << last_list.incomplete_list_items
        list
      end
  	end
  end
end
