class ListItem < ActiveRecord::Base
  attr_accessible :done, :list_id, :name

  belongs_to :list
end
