class Invitation < ActiveRecord::Base
  attr_accessible :friends_email, :key, :status, :user_id, :friend_id

  belongs_to :user
  belongs_to :friend
  before_create :create_key
  after_create :email_friend

  validates :friends_email, :presence=>true, :email => true

  state_machine :status, :initial => :init do
    before_transition :init => :sent, :do => :send_invitation_email
    before_transition :any => :accepted, :do => :send_accepted_email

    event :email_friend do
      transition :init => :sent
    end

  	event :accept do
      transition any => :accepted
  	end

    event :cancel do
      transition any => :cancelled
    end
  end

	def create_key
		random_hex = SecureRandom.hex(6)

		while Invitation.find_by_key("1X#{random_hex}") != nil
			random_hex = SecureRandom.hex(6)
		end

		self.key="1X#{random_hex}"
	end

	def send_invitation_email
		UserNotifier.send_invitation_email(self).deliver
	end

  def send_accepted_email
    UserNotifier.send_accepted_email(self).deliver
  end

  def accept(acceptors_id)

    @invitor = self.user
    @acceptor = User.find(acceptors_id)

    @invitor.partner=@acceptor
    @acceptor.partner=@invitor
    
    @invitor.save
    @acceptor.save

    super

    #puts "#{@acceptor.first_name} just accepted #{user.first_name}'s invitation"
  end

end
