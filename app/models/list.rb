class List < ActiveRecord::Base
  attr_accessible :user_id, :day, :list_items, :list_items_attributes

  validates :user_id, :presence=>true

  belongs_to :user#, :class_name => :User#, :foreign_key=>:user_id
  has_many :list_items
  has_many :incomplete_list_items, :class_name=>'ListItem', :foreign_key=> :list_id, :conditions=> { done: false }

  accepts_nested_attributes_for :list_items, :reject_if => proc { |a| a['name'].blank? }, :allow_destroy => true

  def nice_format_day
  	case self.day
  	when Date.today
  		"today"
  	else
  		self.day
  	end
  end
end
