class UserNotifier < ActionMailer::Base
  default from: "HoldMe2It"
  layout 'table_email'

  def send_invitation_email(invitation)

  	@friends_email = invitation.friends_email
  	@key = invitation.key
  	@user = User.find(invitation.user_id)

  	mail to: @friends_email, subject: "#{@user.first_name} wants you to join his to-do-list on HoldMe2It"

  end

  def send_accepted_email(invitation)

    @user = invitation.user
    @friend = User.find(invitation.friend)

    mail to: @user.email, subject: "#{@user.first_name} has accepted your invitation to join your to-do-list on HoldMe2It!"

  end

  def send_welcome_email(user)
  	@user = user

  	mail to: @friends_email, subject: "#{@user.first_name} wants you to join his to-do-list on HoldMe2It"
  end


end
